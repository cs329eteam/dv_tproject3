---
title: "Tableau Project 3: CO2 Emissions"
author: "John Mitchell, Matt Strasburg, John Dominguez"
date: "November 13, 2016"
output:
  html_document:
    toc: yes
  html_notebook:
    toc: yes
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
##Setup
###Session Info
```{r }
sessionInfo()
```

###ETL Operations
For this project we used a csv containing different countries CO2 emissions by year. Using the following ETL file, we were able to clean up the csv and use the created print statement to create the table in SQL Developer. Using the outputed csv, we were able to add the data to the table so we could use it in our Shiny application.

ETL Operations:
```{r message=FALSE}
  source("../01 Data/R_ETL.R", echo = TRUE)
```

###Summary
Here is a summary of our dataset showing co2 use for each country and year.
```{r, echo = FALSE, message=FALSE}
  source("../01 Data/co2_summary.R")
```
```{r, echo = FALSE}
  summary(df)
```
 Also included is the first few rows from a subset of the data, showing some of the United States' emissions.
```{r, echo = FALSE}
  summary(subset(df,Country=="United States"))
```

##Visualizations


If, upon opening Tableau, you are prompted to find the correct dat file, use the reformatted co2_emissions csv file in our 01 Data folder.

###Tableau Plot 1
Our first Tableau visualization divides countries into two sets: those countries who have high emissions, and those who don't. The default view is to show the countries who do not have high emissions: without the traditional heavyweights like the USA, China, and India, different middle-emission leaders begin to emerge, such as Germany, South Korea, and Saudi Arabia. 

![CO2 Emissions, without highest countries](../02 Tableau/Visual1.png)

###Tableau Plot 2
The following graph compares countries and their CO2 emissions for the year 2012, with the highest emission countries taken out. Countries above 1000 units of CO2 emission were taken out to get a better comparison of the smaller countries. This graph was then used to create sets of countries with high CO2 emissions and medium CO2 emissions.

![CO2 Emissions for 2012](../02 Tableau/CO2 emissions for 2012 wfilters.png)

To go along with the sets of data taken from the first graph, the sets were added to a map to see the locations of the high and medium CO2 emission countries. Below is the map of the high emission countries.

![Map of High CO2 Emissions](../02 Tableau/Map of High CO2 countries.png)

Below is a continuation of the map, except that it is now looking at the medium CO2 emission countries.

![Map of Medium CO2 Emissions](../02 Tableau/Map of Medium CO2 countries.png)

###Tableau Plot 3
Our third plot uses a table calculation (Rank) to rank countries based on their total emissions over their entire history. This allows users to use a filter to show customizable rankings, such as top 10 or top 20. This is shown below.

![Top 10 Emissions](../02 Tableau/Top10.png)

![Top 20 Emissions](../02 Tableau/Top20.png)


###Shiny

Our Shiny app can be found published at https://jtm3433.shinyapps.io/03Shiny/. It has a bar graph with a slider that allows you to cut off the minimum emissions in 2012.

This goes along with our first Tableau visualization. Setting the slider all the way up shows all of the 'high emission' countries, while reducing it allows more to be shown.

![Shiny Slider](../02 Tableau/Visual2.PNG)


